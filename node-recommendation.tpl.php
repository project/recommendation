  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
  <?php if ($node->featured) { ?><div class="featured">featured</div><?php }; ?>
  <?php if ($node->echo_display) { ?><div class="echo"><?php print $node->echo_display; ?></div> <?php } ?>

  <span class="submitted"><?php print $submitted?></span>
  <?php if ($node->author_name) { ?><div class="author_name"><?php print "original author: " . $node->author_name; ?></div> <?php } ?>

  <span class="taxonomy"><?php print $terms?></span>
  <div class="content"><?php print $content?></div>

  <?php if ($links) { ?><div class="links">
  &raquo; 	
  <?php 
    // break up the links if there are more than four because they won't wrap well
    // this could easily be broken with a slight change to drupal's rendering system
    $link_end = "</li>";
    $max_links = 4;

    $pos = strpos($links, $link_end);
    // slighlty cheating here because we are assuming that strpos will not return 0 meaning that
    // needle starts at char 0 of haystack
    for ($i = 1; $i < $max_links && $pos; $i++) {
      $pos = strpos($links, $link_end, $pos + 1);
    }

    if ($pos) {
      print substr($links, 0, $pos + strlen($link_end)) . "<br/>" . substr($links, $pos + strlen($link_end) + 1); 
    }
    else {
      print $links;
    }

  ?>
</div><?php }; ?>
  </div>
